using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostTrigger : MonoBehaviour
{
    public float boostSpeed = 25f;
    public int boostDuration = 3;
    
    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("Player")) return;
        var movementController = other.gameObject.GetComponent<MovementController>();
        StartCoroutine(movementController.Boost(boostSpeed, boostDuration));
    } 
}
