using UnityEngine;

public class LapCheckpoint : MonoBehaviour
{
    public int index;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.GetComponent<LapController>()) return;
        var player = other.gameObject.GetComponent<LapController>();
        
        if (player.checkpointIndex == index - 1)
        {
            player.NextCheckpoint(this); 
        } 

    }
}