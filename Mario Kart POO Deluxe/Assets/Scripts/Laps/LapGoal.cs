using UnityEngine;

public class LapGoal : MonoBehaviour
{
    public int checkpointAmount;
    public int lapAmount;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.GetComponent<LapController>()) return;
        var player = other.gameObject.GetComponent<LapController>();

        if (player.checkpointIndex != checkpointAmount) return;
        
        if (player.lapNumber == lapAmount)
        {
            player.WinRace();
        }
        else
        {
            player.NextLap();
        }
    }
}
