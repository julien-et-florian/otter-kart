using System;
using UnityEngine;

public class LapController : MonoBehaviour
{
    private UserProfileController _profileController;
    private CharacterController _characterController;
    public int lapNumber;
    public int checkpointIndex;
    public LapCheckpoint lastCheckpoint;
    public event Action<int> OnLapChange;
    public event Action<UserProfile> OnRaceEnd;
    
    void Start()
    {
        _profileController = GetComponent<UserProfileController>();
        _characterController = GetComponent<CharacterController>();
        lapNumber = 1;
        checkpointIndex = 0;
    }

    public void NextCheckpoint(LapCheckpoint checkpoint)
    {
        if (checkpoint == null) return;
        lastCheckpoint = checkpoint;
        checkpointIndex = checkpoint.index;
        _profileController.Profile.CheckPointPassed++;
    }

    public void NextLap()
    {
        lapNumber++;
        checkpointIndex = 0;
        OnLapChange?.Invoke(lapNumber);
        _profileController.Profile.LapsFinished++;
    }
    
    public void WinRace()
    {
        _profileController.Profile.LapsFinished++;
        OnRaceEnd?.Invoke(_profileController.Profile);
    }
    
    public void RespawnToLastCheckpoint()
    {
        var (respawnPosition, respawnRotation) = GetRespawnPoint();
        _characterController.enabled = false;
        transform.position = respawnPosition;
        transform.rotation = respawnRotation;
        _characterController.enabled = true;
    }
    
    private Tuple<Vector3, Quaternion> GetRespawnPoint()
    {
        if (lastCheckpoint == null) return GetGoalRespawnPoint(); 
        
        var respawnPoint = lastCheckpoint.transform.GetChild(0);
        return new Tuple<Vector3, Quaternion>(respawnPoint.position, respawnPoint.rotation);
    }
    
    private Tuple<Vector3, Quaternion> GetGoalRespawnPoint()
    {
        var respawnPoint = FindObjectOfType<LapGoal>().transform.GetChild(0); 
        return new Tuple<Vector3, Quaternion>(respawnPoint.position, respawnPoint.rotation);
    }
}
