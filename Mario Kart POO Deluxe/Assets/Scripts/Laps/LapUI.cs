using System;
using TMPro;
using UnityEngine;

public class LapUI : MonoBehaviour
{
    private LapController _lapController;
    public LapController LapController
    {
        get => _lapController;
        set
        {
            _lapController = value; 
            _lapController.OnLapChange += OnLapChange; 
        }
    }
    private LapGoal _lapGoal;
    private TextMeshProUGUI _lapText;

    private void Start()
    {
         _lapGoal = GameObject.Find("Goal").GetComponent<LapGoal>();
         _lapText = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
         _lapText.SetText($"Tour : 1 / {_lapGoal.lapAmount}"); 
    }
    
    public void OnLapChange(int lapNumber)
    {
        _lapText.SetText($"Tour : {lapNumber} / {_lapGoal.lapAmount}");
    }
}
