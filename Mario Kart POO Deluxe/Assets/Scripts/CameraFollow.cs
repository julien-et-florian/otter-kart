using System.Runtime.CompilerServices;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class CameraFollow : MonoBehaviour
{
    public Transform target = null;
    private float smoothSpeed = 0.1f;
    private Vector3 velocity = Vector3.zero;
    
    private void LateUpdate()
    {
        Vector3 desiredPosition = target.position - target.forward*4 + target.up *2.5f;
        Quaternion desiredRotation = target.rotation;
        transform.position = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, smoothSpeed);
        transform.rotation = Quaternion.Lerp(transform.rotation, desiredRotation, smoothSpeed);
    }
}
