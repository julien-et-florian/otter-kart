using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class MovementController : MonoBehaviour
{
    private Animator _animator;
    private CharacterController _controller;

    public bool isGrounded;
    public bool canMove;
    public float jumpForce = 2f;
    private Vector3 playerVelocity;
    public float baseSpeed = 15f;
    public float moveSpeed;
    public float turnSpeed = 100f;
    private float gravityValue = -9.81f;
    public float smoothInputSpeed = .2f;

    private Vector2 CurrentInputVector;
    private Vector2 SmoothInputVelocity;
    
    private Vector2 movementInput = Vector2.zero;
    private bool jumped = false;

    // Use this for initialization
    void Start()
    {
        _animator = GetComponent<Animator>();
        _controller = GetComponent<CharacterController>();
        moveSpeed = baseSpeed;
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        movementInput = context.ReadValue<Vector2>();
    }
    public void OnJump(InputAction.CallbackContext context)
    {
        jumped = context.action.IsPressed();
    }

    void Update()
    {
        if (!canMove) return;
        
        isGrounded = _controller.isGrounded;
        if (isGrounded && playerVelocity.y <0)
        {
            playerVelocity.y = 0f;
        }

        CurrentInputVector =  Vector2.SmoothDamp(CurrentInputVector, movementInput, ref SmoothInputVelocity, smoothInputSpeed);
        float translation = CurrentInputVector.y * moveSpeed;
        float rotation = movementInput.x * turnSpeed;

        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        _controller.Move(translation* _controller.transform.forward);
        
        transform.Rotate(0, rotation, 0);

        var horizontalVelocity = new Vector3(_controller.velocity.x, 0, _controller.velocity.z);

        if (horizontalVelocity.magnitude > 0)
            _animator.SetFloat("moveSpeed", moveSpeed);
        
        if (horizontalVelocity.magnitude < 0)
            _animator.SetFloat("moveSpeed", -moveSpeed);

        if (horizontalVelocity.magnitude == 0)
            _animator.SetFloat("moveSpeed",0);
        

        if (jumped && isGrounded)
        {
            jumped = false;
            _animator.SetFloat("jumpForce", 3f);
            playerVelocity.y += Mathf.Sqrt(jumpForce * -3f * gravityValue);
            StartCoroutine(ResetJumpAnimation(0.67f));
        }

        playerVelocity.y += gravityValue * Time.deltaTime;
        _controller.Move(playerVelocity * Time.deltaTime);
    }

    public IEnumerator ResetJumpAnimation(float duration)
    {
        yield return new WaitForSeconds(duration);
        _animator.SetFloat("jumpForce", 0f);
    }

    private void SetBaseSpeed()
    {
        canMove = true;
        moveSpeed = baseSpeed;
        _animator.SetFloat("moveSpeed", moveSpeed);
    }
    
    public IEnumerator Boost(float boostSpeed, int duration)
    {
        moveSpeed += boostSpeed;
        _animator.SetFloat("moveSpeed", boostSpeed);
        yield return new WaitForSeconds(duration);
        SetBaseSpeed(); 
    }

    public IEnumerator StopMovement(int duration)
    {
        canMove = false;
        _animator.SetFloat("moveSpeed", 0);
        _animator.SetBool("isTouched", true);
        yield return new WaitForSeconds(duration);
        SetBaseSpeed();
        _animator.SetBool("isTouched", false);
    }
}
