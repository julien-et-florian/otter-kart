using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;

public class DisplayStats : MonoBehaviour
{
    private List<UserProfile> _users;
    public Dropdown DropdownPlayer;

    public Text CheckPointPassed;
    public Text LapsFinished;
    public Text ShellsLauched;
    public Text FishEaten;
    public Text BloopersUsed;
    public Text ItemBoxTriggered;
    public Text RacesPlayed;
    public Text Victories;

    void Update()
    {
        UpdateStats();
    }
    void Start()
    {
        UpdateUsers();
    }

    public void MainMenuButton()
    {
        // Go back to menu
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }

    public void UpdateUsers()
    {
        if (File.Exists(Application.streamingAssetsPath + "/CharaData.json"))
        {
            var usersString = File.ReadAllText(Application.streamingAssetsPath + "/CharaData.json");
            _users = JsonConvert.DeserializeObject<List<UserProfile>>(usersString);
            var options = new List<Dropdown.OptionData>();
            _users?.ForEach(user =>
            {
                if (user != null)
                {
                    var element = new Dropdown.OptionData(user?.Username);
                    options.Add(element);
                }
            });
            DropdownPlayer.ClearOptions();
            DropdownPlayer.AddOptions(options);
        }
        else
        {
            File.Create(Application.streamingAssetsPath + "/CharaData.json");
        }
    }

    private void UpdateStats()
    {
        if (DropdownPlayer?.value == null || _users == null) return;
        var user = _users[DropdownPlayer.value];
        CheckPointPassed.text = $"Checkpoint passed : {user.CheckPointPassed}";
        LapsFinished.text = $"Laps finished : {user.LapsFinished}";
        ShellsLauched.text = $"Shells lauched : {user.ShellsLaunched}";
        FishEaten.text = $"Fish eaten : {user.FishEaten}";
        BloopersUsed.text = $"Bloopers used: {user.BloopersUsed}";
        ItemBoxTriggered.text = $"Itembox triggered: {user.ItemBoxesTriggered}";
        RacesPlayed.text = $"Races played : {user.RacesPlayed}";
        Victories.text = $"Victories: {user.Victories}";
    }
}
