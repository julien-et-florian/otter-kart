using UnityEngine;

public class SC_MainMenu : MonoBehaviour
{
    public GameObject MainMenu;
    public GameObject CreditsMenu;
    public GameObject ControlsMenu;

    private AudioManager _audioManager;

    // Start is called before the first frame update
    private void Start()
    {
        _audioManager = FindObjectOfType<AudioManager>();
        PlayMenuTheme();
        MainMenuButton();
    }

    private void PlayMenuTheme()
    {
        _audioManager.PlayTheme("MenuTheme");
    }

    public void GoToAccoutSelection()
    {
        // Play Now Button has been pressed, here you can initialize your game (For example Load a Scene called GameLevel etc.)
        UnityEngine.SceneManagement.SceneManager.LoadScene("AccountSelection");
    }

    public void GoToStats()
    {
        // Play Now Button has been pressed, here you can initialize your game (For example Load a Scene called GameLevel etc.)
        UnityEngine.SceneManagement.SceneManager.LoadScene("DisplayStats");
    }

    public void CreditsButton()
    {
        // Show Credits Menu
        MainMenu.SetActive(false);
        CreditsMenu.SetActive(true);
    }

    public void MainMenuButton()
    {
        // Show Main Menu
        MainMenu.SetActive(true);
        CreditsMenu.SetActive(false);
        ControlsMenu.SetActive(false);
    }

    public void ControlsButton()
    {
        // Show Controls Menu
        MainMenu.SetActive(false);
        ControlsMenu.SetActive(true);
    }

    public void QuitButton()
    {
        // Quit Game
        Application.Quit();
    }
}