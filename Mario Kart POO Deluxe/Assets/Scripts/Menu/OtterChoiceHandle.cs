using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtterChoiceHandle : MonoBehaviour
{
    public const string LilBroChoice = "lilBroChoice";
    public const string BigBroChoice = "bigBroChoice";
    public static OtterChoiceHandle Instance;
    
    public string FirstPlayerChoice { get; set; }
    public string SecondPlayerChoice { get; set; }

    [SerializeField]
    private GameObject lilBroPrefab;
    [SerializeField]
    private GameObject bigBroPrefab;
    

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private GameObject GetPlayerChoicePrefab(string playerChoice)
    {
        return playerChoice switch
        {
            LilBroChoice => lilBroPrefab,
            BigBroChoice => bigBroPrefab,
            _ => null
        };
    }
    
    public GameObject GetFirstPlayerOtterPrefab()
    {
        return GetPlayerChoicePrefab(FirstPlayerChoice);
    }
    
    public GameObject GetSecondPlayerOtterPrefab()
    {
        return GetPlayerChoicePrefab(SecondPlayerChoice);
    }
    
}
