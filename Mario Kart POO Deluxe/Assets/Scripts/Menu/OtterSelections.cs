using UnityEngine;
using UnityEngine.UI;

public class OtterSelections : MonoBehaviour
{
    public Text Player1;
    public Text Player2;
    public Dropdown dropdownPlayer1;
    public Dropdown dropdownPlayer2;
    public Text Stats1;
    public Text Stats2;
    public GameObject lilBroModel1;
    public GameObject lilBroModel2;
    public GameObject bigBroModel1;
    public GameObject bigBroModel2;

    private UserProfilesHandle _userProfilesHandle;
    private OtterChoiceHandle _otterChoiceHandle;

    public void Start()
    {
        _userProfilesHandle = FindObjectOfType<UserProfilesHandle>();
        _otterChoiceHandle = FindObjectOfType<OtterChoiceHandle>();
        Player1.text = _userProfilesHandle.Profiles[0].Username;
        Player2.text = _userProfilesHandle.Profiles[1].Username;
    }

    public void Update()
    {
        CheckDropdownChoice();
    }

    public void CheckDropdownChoice()
    {
        // Player 1 Otter Selection
        if (dropdownPlayer1.value == 0)
        {
            Stats1.text = "Stats: best acceleration, best maniability, normal speed";
            lilBroModel1.SetActive(true);
            bigBroModel1.SetActive(false);
            _otterChoiceHandle.FirstPlayerChoice = OtterChoiceHandle.LilBroChoice;
        }
        else
        {
            Stats1.text = "Stats: best speed, low acceleration, low maniability";
            lilBroModel1.SetActive(false);
            bigBroModel1.SetActive(true);
            _otterChoiceHandle.FirstPlayerChoice = OtterChoiceHandle.BigBroChoice;
        }
        // Player 2 Otter Selection
        if (dropdownPlayer2.value == 0)
        {
            Stats2.text = "Stats: best acceleration, best maniability, normal speed";
            lilBroModel2.SetActive(true);
            bigBroModel2.SetActive(false);
            _otterChoiceHandle.SecondPlayerChoice = OtterChoiceHandle.LilBroChoice;
        }
        else
        {
            Stats2.text = "Stats: best speed, low acceleration, low maniability";
            lilBroModel2.SetActive(false);
            bigBroModel2.SetActive(true);
            _otterChoiceHandle.SecondPlayerChoice = OtterChoiceHandle.BigBroChoice;
        }
    }

    public void PlayNowButton()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("DinoDinoJungle");
    }

    public void MainMenuButton()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("AccountSelection");
    }
}
