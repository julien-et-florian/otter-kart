using TMPro;
using UnityEngine;

public class EndMenu : MonoBehaviour
{
    public TextMeshProUGUI winnerUsernameText;
    private RaceResultsHandle _raceResultsHandle;
    
    private void Start()
    {
        _raceResultsHandle = FindObjectOfType<RaceResultsHandle>();
        DisplayWinner();
    }
    
    private void DisplayWinner()
    {
        winnerUsernameText.text = _raceResultsHandle.GetWinnerUsername();
    }
    
    public void MainMenuButton()
    {
        // Go back to menu
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }

    public void QuitButton()
    {
        Application.Quit();
    }
}
