using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;

public class AccountSelection : MonoBehaviour
{
    public Dropdown dropdownPlayer1;
    public Dropdown dropdownPlayer2;
    public Text ErrorText;
    public InputField nameInput;

    private UserProfilesHandle _userProfilesHandle;
    private List<UserProfile> _users;

    public void Start()
    {
        _userProfilesHandle = FindObjectOfType<UserProfilesHandle>();
        UpdateUsers();
    }

    public void UpdateUsers()
    {
        if (File.Exists(Application.streamingAssetsPath + "/CharaData.json"))
        {
            var usersString = File.ReadAllText(Application.streamingAssetsPath + "/CharaData.json");
            _users = JsonConvert.DeserializeObject<List<UserProfile>>(usersString);
            var options = new List<Dropdown.OptionData>();
            _users?.ForEach(user =>
            {
                if (user != null)
                {
                    var element = new Dropdown.OptionData(user?.Username);
                    options.Add(element);
                }
            });
            dropdownPlayer1.ClearOptions();
            dropdownPlayer1.AddOptions(options);
            dropdownPlayer2.ClearOptions();
            dropdownPlayer2.AddOptions(options);
        }
        else
        {
            File.Create(Application.streamingAssetsPath + "/CharaData.json");
        }
    }

    public void PlayNowButton()
    {
        if (dropdownPlayer1.value == dropdownPlayer2.value)
        {
            ErrorText.gameObject.SetActive(true);
        }
        else
        {
            _userProfilesHandle.AddProfile(_users[dropdownPlayer1.value]);
            _userProfilesHandle.AddProfile(_users[dropdownPlayer2.value]);
            UnityEngine.SceneManagement.SceneManager.LoadScene("OtterSelectionMenu");
        }
    }

    public void MainMenuButton()
    {
        // Go back to menu
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }

    public void CreatePlayerButton()
    {
        var name = nameInput.text;
        var user = new UserProfile(name);
        var usersString = File.ReadAllText(Application.streamingAssetsPath + "/CharaData.json");
        var users = JsonConvert.DeserializeObject<List<UserProfile>>(usersString);
        if (users==null)
        {
            users = new List<UserProfile>();
        }
        users.Add(user);
        var usersJson = JsonConvert.SerializeObject(users);
        File.WriteAllText(Application.streamingAssetsPath + "/CharaData.json", usersJson);
        UpdateUsers();
    }
}
