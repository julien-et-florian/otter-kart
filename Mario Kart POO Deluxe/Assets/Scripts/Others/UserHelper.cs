using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json;
using UnityEngine;

public static class UserHelper 
{
    private static List<UserProfile> GetUsersOrCreateFile()
    {
        if (File.Exists(Application.streamingAssetsPath + "/CharaData.json"))
        {
            var usersString = File.ReadAllText(Application.streamingAssetsPath + "/CharaData.json");
            return JsonConvert.DeserializeObject<List<UserProfile>>(usersString);
        }
        File.Create(Application.streamingAssetsPath + "/CharaData.json");
        return null;
    }

    public static void UpdateUser(UserProfile user)
    {
        var users = GetUsersOrCreateFile();
        if (users == null) return;
        var savedUser = users.First(x => x.Username == user.Username);
        users.Remove(savedUser);
        users.Add(user);
        var usersJson = JsonConvert.SerializeObject(users);
        File.WriteAllText(Application.streamingAssetsPath + "/CharaData.json", usersJson);
    }
}
