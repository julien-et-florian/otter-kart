using System;
using UnityEngine.InputSystem;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public GameObject firstPlayerCanvas;
    public GameObject secondPlayerCanvas;
    private PlayerInputManager _playerInputManager;
    private UserProfilesHandle _userProfilesHandle;
    private RaceResultsHandle _raceResultsHandle;
    
    private void Awake()
    {
        _playerInputManager = GetComponent<PlayerInputManager>();
        _playerInputManager.onPlayerJoined += OnPlayerJoined;
        _userProfilesHandle = FindObjectOfType<UserProfilesHandle>();
        _raceResultsHandle = FindObjectOfType<RaceResultsHandle>();
    }

    private void OnPlayerJoined(PlayerInput playerInput)
    {
        BindPlayerUI(playerInput);
        BindPlayerUserProfile(playerInput);
        BindPlayerRaceEndEvent(playerInput);
    }

    private GameObject GetPlayerCanvas(int playerIndex)
    {
        return playerIndex switch
        {
            0 => firstPlayerCanvas,
            1 => secondPlayerCanvas,
            _ => throw new ArgumentException("Player index must be 0 or 1")
        };
    }
    
    private void BindPlayerUI(PlayerInput playerInput)
    {
        var canvas = GetPlayerCanvas(playerInput.playerIndex);
        
        var itemUI = canvas.GetComponentInChildren<ItemSlotUI>();
        var itemManager = playerInput.GetComponent<ItemManager>();
        
        var lapUI = canvas.GetComponentInChildren<LapUI>();
        var lapController = playerInput.GetComponent<LapController>();
        
        itemUI.itemManager = itemManager;
        lapUI.LapController = lapController;
    }

    private void BindPlayerUserProfile(PlayerInput playerInput)
    {
        var userProfile = _userProfilesHandle.Profiles[playerInput.playerIndex];
        var userProfileController = playerInput.GetComponent<UserProfileController>();
        userProfileController.SetProfile(userProfile);
    }
    
    private void BindPlayerRaceEndEvent(Component playerInput)
    {
        var lapController = playerInput.GetComponent<LapController>();
        lapController.OnRaceEnd += _raceResultsHandle.OnRaceEnd;
        lapController.OnRaceEnd += _userProfilesHandle.OnRaceEnd;
    }
}
