using UnityEngine;

public class UserProfileController : MonoBehaviour
{
    public UserProfile Profile;
    
    public void SetProfile(UserProfile profile)
    {
        Profile = profile;
    }

}
