public class UserProfile
{
    public string Username { get; set; }
    public int CheckPointPassed { get; set; }
    public int LapsFinished { get; set; }
    public int ShellsLaunched { get; set; }
    public int FishEaten { get; set; }
    public int BloopersUsed { get; set; }
    public int ItemBoxesTriggered { get; set; }
    public int RacesPlayed { get; set; }
    public int Victories { get; set; }

    public UserProfile(string username)
    {
        Username = username;
        CheckPointPassed = 0;
        LapsFinished = 0;
        ShellsLaunched = 0;
        FishEaten = 0;
        BloopersUsed = 0;
        ItemBoxesTriggered = 0;
        RacesPlayed = 0;
        Victories = 0;
    }
}
