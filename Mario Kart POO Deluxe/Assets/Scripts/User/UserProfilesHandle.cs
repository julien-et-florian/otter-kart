using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UserProfilesHandle : MonoBehaviour
{
    public static UserProfilesHandle Instance;
    public List<UserProfile> Profiles { get; private set; } 

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        
        DontDestroyOnLoad(gameObject);
        Profiles = new List<UserProfile>();
    }
    
    public void AddProfile(UserProfile profile)
    {
        if (!Profiles.Contains(profile)) 
            Profiles.Add(profile);
    }
    
    public void OnRaceEnd(UserProfile winner)
    {
        winner.Victories++;
        foreach (var profile in Profiles)
        {
            profile.RacesPlayed++;
            UserHelper.UpdateUser(profile);
        }
    }
}
