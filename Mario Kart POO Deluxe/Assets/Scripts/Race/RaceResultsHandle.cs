using UnityEngine;
using UnityEngine.SceneManagement;

public class RaceResultsHandle : MonoBehaviour
{
    public static RaceResultsHandle Instance;
    
    private UserProfile Winner;
    private const string EndSceneKey = "EndScene";
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
    
    public void OnRaceEnd(UserProfile winner)
    {
        Winner = winner;
        LoadEndScene();
    }
    
    public string GetWinnerUsername() => Winner.Username;
    
    private void LoadEndScene()
    {
        SceneManager.LoadScene(EndSceneKey);
    }
}
