using System.Collections;
using TMPro;
using UnityEngine;

public class RaceInitializer : MonoBehaviour
{
    public TextMeshProUGUI countDownText;
    public GameObject firstPlayerSpawn;
    public GameObject secondPlayerSpawn;

    private GameObject _firstPlayer;
    private GameObject _secondPlayer;

    private OtterChoiceHandle _otterChoiceHandle;
    private AudioManager _audioManager;
    
    void Start()
    {
        _otterChoiceHandle = FindObjectOfType<OtterChoiceHandle>();
        _audioManager = FindObjectOfType<AudioManager>();
        PlayMapTheme();
        PlayStartSound();
        SpawnPlayers();
        StartCoroutine(CountDown());
    }

    private void SpawnPlayers()
    {
        _firstPlayer = Instantiate(_otterChoiceHandle.GetFirstPlayerOtterPrefab());
        TeleportPlayerToSpawn(_firstPlayer, firstPlayerSpawn);
        
        _secondPlayer = Instantiate(_otterChoiceHandle.GetSecondPlayerOtterPrefab());
        TeleportPlayerToSpawn(_secondPlayer, secondPlayerSpawn);
    }
    
    private void TeleportPlayerToSpawn(GameObject player, GameObject spawn)
    {
        var characterController = player.GetComponentInChildren<CharacterController>();
        characterController.enabled = false;
        characterController.transform.position = spawn.transform.position;
        characterController.transform.rotation = spawn.transform.rotation;
        characterController.enabled = true;
    }
    
    private IEnumerator CountDown()
    {
        countDownText.text = "3";
        yield return new WaitForSeconds(1);
        countDownText.text = "2";
        yield return new WaitForSeconds(1);
        countDownText.text = "1";
        yield return new WaitForSeconds(1);
        countDownText.text = "START !";
        UpdateCanMoveFromPlayers();
        yield return new WaitForSeconds(1);
        countDownText.gameObject.SetActive(false);
    }

    private void PlayMapTheme()
    {
        _audioManager.PlayTheme("DinoDinoJungleTheme");
    }
    
    private void PlayStartSound()
    {
        _audioManager.Play("RaceStart");
    }
    private void UpdateCanMoveFromPlayers()
    {
        _firstPlayer.GetComponentInChildren<MovementController>().canMove = true;
        _secondPlayer.GetComponentInChildren<MovementController>().canMove = true;
    }

}
