using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public class ItemManager : MonoBehaviour
{
    private MovementController _controller;
    private UserProfileController _profileController;
    private AudioManager _audioManager;
    private ItemsHandle _handle;
    private GameObject _firePoint;

    public float delayBeforeItemPickup = 1;
    
    public bool canPickup;
    public int heldItemIndex; 
    public Item currentItem;

    private bool _usingObject;

    private void Start()
    {
        _handle = FindObjectOfType<ItemsHandle>();
        _audioManager = FindObjectOfType<AudioManager>();
        _controller = GetComponent<MovementController>();
        _profileController = GetComponent<UserProfileController>();
        _firePoint = transform.Find("FirePoint").gameObject;
        
        ResetItem();
    }

    public void OnUse(InputAction.CallbackContext context)
    {
        _usingObject = context.action.IsPressed();
    }
    private void Update()
    {
        if (_usingObject && heldItemIndex != -1)
        {
            ActivateItem(); 
        }
    }

    public void StartPickUp()
    {
        StartCoroutine(PickUp());
    }
    
    private IEnumerator PickUp()
    {
        if (heldItemIndex == -1 && canPickup)
        {
            canPickup = false;

            yield return new WaitForSeconds(delayBeforeItemPickup);
            
            int randomItemIndex = Random.Range(0, _handle.allItems.Length);
            currentItem = _handle.allItems[randomItemIndex];
            heldItemIndex = randomItemIndex;
            _profileController.Profile.ItemBoxesTriggered++; 
        } 
    }

    private void MockPickUp(string itemName)
    {
       currentItem = _handle.allItems.FirstOrDefault(item => item.Name == itemName);
       heldItemIndex = _handle.allItems.ToList().IndexOf(currentItem);
    }
    
    private void ActivateItem()
    {
        switch (currentItem.Name)
        {
            case ItemsHandle.FishItem:
                ActivateFishItem();
                break;
            case ItemsHandle.GreenShellItem:
                ActivateGreenShellItem();
                break;
            case ItemsHandle.BlooperItem:
                ActivateBlooperItem();
                break;
        }
        ResetItem();
    }

    private void ActivateFishItem()
    {
        var fish = (BoostItem) currentItem;
        StartCoroutine(_controller.Boost(fish.boostSpeed, fish.duration));
        _audioManager.Play("Fish");
        _profileController.Profile.FishEaten++;
    }

    private void ActivateGreenShellItem()
    {
        var greenShell = (LaunchableItem) currentItem;
        Instantiate(greenShell.launchablePrefab, _firePoint.transform.position, _firePoint.transform.rotation);
        _profileController.Profile.ShellsLaunched++;
    }
    
    private void ActivateBlooperItem()
    {
        StartCoroutine(_handle.blooper.ActivateEffect());
        _audioManager.Play("InkSplash");
        _profileController.Profile.BloopersUsed++;
    }

    private void ResetItem()
    {
        currentItem = null;
        heldItemIndex = -1;
        canPickup = true;
    }
}
