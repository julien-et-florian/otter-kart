using UnityEngine;

[CreateAssetMenu(fileName = "New LaunchableItem", menuName = "Items/LaunchableItem")]
public class LaunchableItem : Item 
{
    public GameObject launchablePrefab;
}