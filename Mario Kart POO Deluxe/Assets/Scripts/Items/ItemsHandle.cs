using UnityEngine;

public class ItemsHandle : MonoBehaviour
{
    public const string FishItem = "Fish";
    public const string GreenShellItem = "Green Shell";
    public const string BlooperItem = "Blooper";
    
    public Item[] allItems;
    public Blooper blooper;
}
