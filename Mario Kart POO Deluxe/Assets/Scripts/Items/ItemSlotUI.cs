using UnityEngine;
using UnityEngine.UI;

public class ItemSlotUI : MonoBehaviour
{
    public ItemManager itemManager;
    private bool _shouldShuffle = true;
    public float timeBetweenShuffle = 0.10f;
    public Sprite[] itemSprites;

    public Image itemImage;
    
    private void Update()
    {
        if (itemManager == null) return;
        
        if (itemManager.heldItemIndex == -1)
        {
            if (itemManager.canPickup)
            {
                itemImage.sprite = null;
            }
            else
            {
                if (_shouldShuffle)
                {
                    Invoke(nameof(Shuffle), timeBetweenShuffle);
                    _shouldShuffle = false;
                }
            }
        }
        else
        {
            itemImage.sprite = itemManager.currentItem.Icon;
        }
    }

    private void Shuffle()
    {
        itemImage.sprite = itemSprites[Random.Range(0, itemSprites.Length)];
        _shouldShuffle = true;
    }
}
