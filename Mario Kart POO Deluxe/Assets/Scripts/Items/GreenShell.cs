using System;
using System.Collections;
using UnityEngine;

public class GreenShell : MonoBehaviour
{
    private const float ShellAnimationLength = 0.3f;
    
    public float shellSpeed;
    public int maxRebound = 5;
    public int stunTime = 2;
  
    private int _reboundCount = 0;
    private ParticleSystem _shellParticle;
    private MeshRenderer _shellRenderer;
    private bool shouldMove = true;

    private void Start()
    {
        _shellParticle = GetComponentInChildren<ParticleSystem>();
        _shellRenderer = GetComponentInChildren<MeshRenderer>();
    }

    void FixedUpdate()
    {
        if (shouldMove)
        {
            transform.position += transform.forward * shellSpeed * Time.fixedDeltaTime; 
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            var playerController = collision.gameObject.GetComponent<MovementController>();
            playerController.StartCoroutine(playerController.StopMovement(stunTime));
            DestroyGreenShell();
            return;
        } 
        
        foreach (ContactPoint contact in collision.contacts)
        {
            Vector3 reflectDir = Vector3.Reflect(transform.forward, contact.normal);
            transform.LookAt(reflectDir + transform.position);
        } 
        _reboundCount++;
        
        if (_reboundCount >= maxRebound)
        {
            DestroyGreenShell();
        }
    }
    
    private void DestroyGreenShell()
    {
        shouldMove = false;
        _shellRenderer.enabled = false;
        _shellParticle.Play();
        Destroy(gameObject, ShellAnimationLength);
    }
}
