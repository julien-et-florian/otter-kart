using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Blooper : MonoBehaviour
{
    public float effectDuration = 5f;
    private Image _inkImage;

    private void Start()
    {
        _inkImage = GetComponent<Image>();
        _inkImage.enabled = false;
    }

    public IEnumerator ActivateEffect()
    {
        _inkImage.enabled = true;
        yield return new WaitForSeconds(effectDuration);
        _inkImage.enabled = false;
    }
}
