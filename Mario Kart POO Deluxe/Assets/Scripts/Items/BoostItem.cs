using UnityEngine;

[CreateAssetMenu(fileName = "New BoostItem", menuName = "Items/BoostItem")]
public class BoostItem : Item 
{
    public float boostSpeed;
    public int duration;
}
