using System.Collections;
using UnityEngine;

public class ItemBox : MonoBehaviour
{
    private const float ItemBoxRespawnDelay = 10f;
    
    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("Player")) return;
        
        var itemManager = other.gameObject.GetComponent<ItemManager>();
        var audioManager = FindObjectOfType<AudioManager>();
        if (itemManager.heldItemIndex == -1 && itemManager.canPickup)
        {
            itemManager.StartPickUp();
            audioManager.Play("ItemBox");
            itemManager.StartCoroutine(DisableTemporarily());
        }
    }
    private IEnumerator DisableTemporarily()
    {
        gameObject.SetActive(false);
        yield return new WaitForSeconds(ItemBoxRespawnDelay);
        gameObject.SetActive(true);
    } 
}
