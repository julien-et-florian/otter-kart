![titleLogo](images/menu_logo.png)

# Description

Ce project est un exercice final réalisé dans le cadre du module de programmation orienté objet à l'EI CNAM (Promo 2021-2024).

L'objectif était de réaliser un jeu de course dans le style mario kart en C# à l'aide du moteur de jeu unity.

Particularité de notre projet : Vous contrôlez des louttres et non des voitures !   
Le jeu est prévu pour être joué à 2 joueurs uniquement (un au clavier et l'autre a la manette).   
Vous aurez le choix entre 2 loutres ayant des statistiques différentes (chaque joueur peut choisir celle qui veut).

Vous trouverez sur le circuit des boîtes d'objets, n'hésitez pas a les ramasser pour récupérer un bonus qui vous sera (peut-être) utile.

# Screenshots

![screenshot1](images/Screenshot1.jpg)
![screenshot2](images/Screenshot2.jpg)
![screenshot3](images/Screenshot3.png)

# Installation

Le jeu est disponible dans l'archive zip "otter-kart-build.zip" situé à la racine du projet.   
Il suffit de l'extraire et de lancer l'éxécutable "Otter Kart Ultra Deluxe.exe" pour lancer une partie.

# Auteurs

Julien Dubocage & Florian Metz